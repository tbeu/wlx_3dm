Rhinoceros® Preview plugin 2.0.0.0 for Total Commander
======================================================

 * License:
-----------

Copyright (C) 2007-2021 by Thomas Beutlich

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


 * Installation:
----------------

Open the plugin archive using Total Commander and installation will start.
The default configuration file 3dm.ini is initialized during the very first
plugin usage.


 * Description:
---------------

Rhinoceros stores a preview bitmap for 3dm files. This is typically shown in
the File-Open dialog of Rhinoceros.

The Rhinoceros Preview plugin shows the embedded preview bitmap of Rhino 3-D
models. It can also be used for the thumbnail view of Total Commander >= 6.5.

Extracting the preview bitmap does not require Rhinoceros to be installed.

The default background color can be set in section [Rhino Preview Settings]
of 3dm.ini.

The optional status bar displays the Rhino interface information.


 * ChangeLog:
-------------

 o Version 2.0.0.0 (16.02.2021)
   - built with openNURBS Toolkit v7.0.20255.14200
   - added optional status bar
   - removed options Width and Height from section [Rhino Preview Settings]
     of 3dm.ini
   - fixed memory leaks
   - switched to Visual Studio 2017
   - added .clang-format
   - removed MPRESS binary compression
   - released as open-source
 o Version 1.1.0.0 (26.09.2011)
   - added Unicode support of files names (for Total Commander >= 7.50)
   - added print preview bitmap (CTRL+P)
   - changed directory of 3dm.ini from plugin directory to same directory
     as wincmd.ini
   - added 64 bit support (for Total Commander >= 8 beta 1)
 o Version 1.0.2.0 (16.04.2007)
   - fixed vertical scrolling
 o Version 1.0.1.0 (02.02.2007)
   - fixed: avoid temporary saving of preview bitmap to file
   - added: copy preview bitmap to clipboard (CTRL+C)
 o Version 1.0.0.1 (23.01.2007)
   - first public version


 * References:
--------------

 o Guidelines Support Library
   - https://github.com/microsoft/GSL
 o LS-Plugin Writer's Guide by Christian Ghisler
   - https://ghisler.github.io/WLX-SDK
 o openNURBS Toolkit by openNURBS Initiative
   - https://github.com/mcneel/opennurbs


 * Acknowledgments:
-------------------

 o Get3dmProperties by Mike Dejanovic
   - http://www.rhino3d.e-cnc.com/3dm_properties/3dm_properties.htm


 * Trademark and Copyright Statements:
--------------------------------------

 o openNURBS is a trademark of Robert McNeel & Associates.
   Copyright © 1993-2021 by Robert McNeel & Associates. All rights reserved.
   - http://www.openNURBS.org
 o Rhinoceros is a registered trademark of Robert McNeel & Associates.
   Copyright © 1993-2021 by Robert McNeel & Associates. All rights reserved.
   - http://www.rhino3d.com
 o Total Commander is Copyright © 1993-2021 by Christian Ghisler, Ghisler Software GmbH.
   - http://www.ghisler.com


 * Feedback:
------------

If you have problems, questions, suggestions please contact Thomas Beutlich.
 o Email: support@tbeu.de
 o URL: http://tbeu.totalcmd.net